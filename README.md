## Prerequisite:
Dokcer Desktop
Python version 3.7 or above

## SETUP:

### installing libraries:
open terminal and enter:
		
		pip install -r requirements.txt
		

### setting up elastic search and localstack:
open terminal and cd to  rootfolder enter:
	
		docker-compose up
		example:
			cd projone
			docker-compose up

### Server UP:
open terminal and cd to  rootfolder enter:
	
		python manage.py runserver		